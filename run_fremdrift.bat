@echo off 

REM set vars
SET PGCLIENTENCODING=UTF8
SET PGPASSWORD=%DB_ADMIN_PASS%
SET pg_hostname=10.33.131.50
SET pg_username=%DB_ADMIN_USER%
SET pg_port=5432
SET dbname=grukos
SET path_files=%~dp0

cd C:\PostgreSQL\10\bin
psql -h %pg_hostname% -p %pg_port% -U %pg_username% -d %dbname% -f %path_files%\daily_progress_report.sql
psql -h %pg_hostname% -p %pg_port% -U %pg_username% -d %dbname% -f %path_files%\daily_status_indmeldinger.sql

call conda activate fremdrift_rapport
python %path_files%\plot_iol_prognosis.py
python %path_files%\plot_status_paa_indmeldte_opgaver.py
