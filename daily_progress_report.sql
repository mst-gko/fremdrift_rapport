BEGIN;
	
	CREATE SCHEMA IF NOT EXISTS fremdrift;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA fremdrift IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak Schema containing progress reports to the the Executive Board'
			     || '''';
		END
	$do$
	;

	DO
	$$
		BEGIN
			EXECUTE format('DROP TABLE IF EXISTS fremdrift.%I CASCADE', 'fremdrift_' || to_char(current_timestamp,'YYYY_MM_DD'));
			EXECUTE format
				(
				    '
						CREATE TABLE fremdrift.%I AS
						WITH 
					        months AS 
					            (
					                SELECT 
					                    to_char(to_timestamp (m::text, ''MM''), ''Mon'') AS months,
					                    date_part(''month'', to_timestamp (m::text, ''MM'')) AS months_int
					                FROM generate_series(1, 12, 1) s(m)
					            ),
					        styr_prog AS 
					            (
					                SELECT 
					                    *
					                FROM gvkort.styringsvaerktoej s 
					                WHERE EXTRACT(YEAR FROM forv_afsl) = EXTRACT(YEAR FROM current_date)
					                    AND prioritering_kat in (''Direkte rettelser'', ''Skal-opgave'', ''Synergi m. skal-opgave'')
					            ),
					        count_prog AS
					            (
					                SELECT 
					                    m.months, 
					                    months_int,
					                    count(s.*) AS counts
					                FROM months m
					                LEFT JOIN styr_prog s ON m.months = TO_CHAR(s.forv_afsl, ''Mon'')
					                GROUP BY m.months, months_int
					                ORDER BY months_int
					            ),
					        cs_prog AS 
					            (
					                SELECT 
					                    months,
					                   	months_int,
					                    counts,
					                    SUM(counts) OVER (ORDER BY months_int) AS cum_sum_prog
					                FROM count_prog
					                ORDER BY months_int
					            ),       
					        styr_done AS 
					            (
					                SELECT 
					                    *
					                FROM gvkort.styringsvaerktoej s 
					                WHERE EXTRACT(YEAR FROM dato_afsluttet) = EXTRACT(YEAR FROM current_date)
					                    AND EXTRACT(MONTH FROM dato_afsluttet) <= EXTRACT(MONTH FROM current_date) 
					                    AND prioritering_kat in (''Direkte rettelser'', ''Skal-opgave'', ''Synergi m. skal-opgave'')
					            ),
					        count_done AS
					            (
					                SELECT 
					                    m.months, 
					                    months_int,
					                    count(*) AS counts
					                FROM months m
					                LEFT JOIN styr_done s ON m.months = TO_CHAR(s.dato_afsluttet, ''Mon'')
					                WHERE EXTRACT(YEAR FROM dato_afsluttet) = EXTRACT(YEAR FROM current_date)
					                GROUP BY months, months_int
					                ORDER BY months_int
					            ),
					        count_done_2 AS 
					            (
					                SELECT 
					                    m.months, 
					                    m.months_int, 
					                    COALESCE(counts, 0) AS counts
					                FROM months m
					                LEFT JOIN count_done cd ON m.months = cd.months
					                WHERE EXTRACT (MONTH FROM current_date ) > m.months_int
					            ), 
					        cs_done AS 
					            (
					                SELECT 
					                	months_int,
					                    months,
					                    counts,
					                    SUM(counts) OVER (ORDER BY cd.months_int) AS cum_sum_done
					                FROM count_done_2 cd 
					                ORDER BY months_int
					            )
					
					    SELECT 
					        p.months, 
					        p.cum_sum_prog, 
					        d.cum_sum_done, 
					        COALESCE(d.cum_sum_done, p.cum_sum_prog) AS cum_sum_mix
					    FROM cs_prog p
					    LEFT JOIN cs_done d ON p.months = d.months 
					    ORDER BY p.months_int
				    '
					, 'fremdrift_' || to_char(current_timestamp,'YYYY_MM_DD')
				);
			EXECUTE format('CREATE OR REPLACE VIEW fremdrift.fremdrift_recent AS SELECT * FROM fremdrift.%I', 'fremdrift_' || to_char(current_timestamp,'YYYY_MM_DD'));
   		END;
	$$ LANGUAGE plpgsql;
	
	DO
	$do$
		BEGIN
			EXECUTE 
				'COMMENT ON TABLE fremdrift.' ||'fremdrift_' || to_char(current_timestamp,'YYYY_MM_DD') || ' IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak Table containing the daily progress report to the the Executive Board'
			     || '''';
		END
	$do$
	;
	
	DO
	$do$
		BEGIN
			EXECUTE 
				'COMMENT ON VIEW fremdrift.fremdrift_recent' || ' IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak VIEW containing the most recent daily progress report to the the Executive Board'
			     || '''';
		END
	$do$
	;
	
	GRANT USAGE ON SCHEMA fremdrift TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA fremdrift TO grukosreader;

COMMIT; 