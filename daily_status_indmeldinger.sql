BEGIN;
	
	CREATE SCHEMA IF NOT EXISTS fremdrift;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA fremdrift IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak Schema containing progress reports to the the Executive Board'
			     || '''';
		END
	$do$
	;

	DO
	$$
		BEGIN
			EXECUTE format('DROP TABLE IF EXISTS fremdrift.%I CASCADE', 'indmeldinger_' || to_char(current_timestamp,'YYYY_MM_DD'));
			EXECUTE format
				(
				    '
						CREATE TABLE fremdrift.%I AS
						WITH 
						alle_opgaver_per_yr AS 
							(
								SELECT 
									EXTRACT(YEAR FROM s.indmeldt_tidspkt) AS yr_indmeld, 
									count(*) AS alle_opg_per_yr
								FROM gvkort.styringsvaerktoej s
								GROUP BY yr_indmeld 
							),
						skalopgaver_per_yr AS 
							(
								SELECT 
									EXTRACT(YEAR FROM s.indmeldt_tidspkt) AS yr_indmeld, 
									count(*) AS opg_per_yr
								FROM gvkort.styringsvaerktoej s
								WHERE prioritering_kat IN (''Skal-opgave'', ''Synergi m. skal-opgave'', ''Direkte rettelser'')
								GROUP BY yr_indmeld 
								ORDER BY yr_indmeld
							),
						direkte_rettelser_per_yr AS 
							(
								SELECT 
									EXTRACT(YEAR FROM s.indmeldt_tidspkt) AS yr_direkte, 
									count(*) AS dir_per_yr
								FROM gvkort.styringsvaerktoej s
								WHERE prioritering_kat IN (''Direkte rettelser'')
								GROUP BY yr_direkte
							),
						afsl_per_yr AS 
							(
								SELECT 
									EXTRACT(YEAR FROM s.dato_afsluttet) AS yr_afslut, 
									count(*) AS afsl_per_yr
								FROM gvkort.styringsvaerktoej s
								WHERE prioritering_kat IN (''Skal-opgave'', ''Synergi m. skal-opgave'', ''Direkte rettelser'')
								GROUP BY yr_afslut
							),
						sum_all AS 
							(
								SELECT 
									''Sum'' AS aar,
									count(*) AS opgaver_aarligt,
									sum(CASE WHEN prioritering_kat IN (''Skal-opgave'', ''Synergi m. skal-opgave'', ''Direkte rettelser'') THEN 1 ELSE 0 END) AS skalopgaver_aarligt,
									sum(CASE WHEN prioritering_kat IN (''Direkte rettelser'') THEN 1 ELSE 0 END) AS direkte_rettelser_aarligt,
									sum(CASE WHEN dato_afsluttet IS NOT NULL THEN 1 ELSE 0 END) AS afsluttede_skalopgaver_aarligt,
									sum(CASE WHEN dato_afsluttet IS NULL THEN 1 ELSE 0 END) AS ikke_afsluttede_kumuleret
								FROM gvkort.styringsvaerktoej s
							)
						SELECT 
							yr_indmeld::TEXT AS aar,
							alle_opg_per_yr AS opgaver_aarligt,
							opg_per_yr AS skalopgaver_aarligt,
							dir_per_yr AS direkte_rettelser_aarligt,
							afsl_per_yr AS afsluttede_skalopgaver_aarligt,
							sum(opg_per_yr - afsl_per_yr) OVER (ORDER BY yr_indmeld) AS ikke_afsluttede_kumuleret
						FROM skalopgaver_per_yr o
						INNER JOIN alle_opgaver_per_yr ao USING (yr_indmeld)
						INNER JOIN afsl_per_yr a ON a.yr_afslut = o.yr_indmeld
						LEFT JOIN direkte_rettelser_per_yr d ON o.yr_indmeld = d.yr_direkte
						UNION ALL 
						SELECT
							*
						FROM sum_all
				    '
					, 'indmeldinger_' || to_char(current_timestamp,'YYYY_MM_DD')
				);
			EXECUTE format('CREATE OR REPLACE VIEW fremdrift.indmeldinger_recent AS SELECT * FROM fremdrift.%I', 'indmeldinger_' || to_char(current_timestamp,'YYYY_MM_DD'));
   		END;
	$$ LANGUAGE plpgsql;
	
	DO
	$do$
		BEGIN
			EXECUTE 
				'COMMENT ON TABLE fremdrift.' ||'indmeldinger_' || to_char(current_timestamp,'YYYY_MM_DD') || ' IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak Table containing the daily count of assignments to the the Executive Board'
			     || '''';
		END
	$do$
	;
	
	DO
	$do$
		BEGIN
			EXECUTE 
				'COMMENT ON VIEW fremdrift.indmeldinger_recent' || ' IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak VIEW containing the most recent daily count of assignments to the the Executive Board'
			     || '''';
		END
	$do$
	;
	
	GRANT USAGE ON SCHEMA fremdrift TO grukosreader;
    GRANT SELECT ON ALL TABLES IN SCHEMA fremdrift TO grukosreader;

COMMIT; 