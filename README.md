# Fremdrift Rapport
Used for generating progress reports to the the Executive Board

## Content
- daily_progress_report.sql: sql script making daily progress reports: 1) prognose, 2) completed and 3) completed with projection from the prognose.
- plot_iol_prognosis.py: python file to generate figures from daily_progress_report.sql 
- daily_status_indmeldinger.sql: sql script making daily report of the registrations of MST-GKO assignments.
- plot_status_paa_indmeldte_opgaver.py: python file to generate figures from daily_status_indmeldinger.sql 
- run_fremdrift.bat: Batch script running the sql and python files