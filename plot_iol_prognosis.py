from datetime import datetime
import os
import configparser

import psycopg2 as pg  # pip install psycogp2
import pandas as pd  # pip install pandas
import matplotlib.pyplot as plt  # pip install matplotlib


class Database:
    def __init__(self, ini_file='F:/GKO/data/grukos/db_credentials/writer/writer.ini'):
        self.ini_file = ini_file

    def parse_db_credentials(self):
        """
        Fetch database credentials for grukos database
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config['GRUKOS']['userid']
        pw_read = config['GRUKOS']['password']
        host = config['GRUKOS']['host']
        port = config['GRUKOS']['port']
        dbname = config['GRUKOS']['databasename']

        return host, port, dbname, usr_read, pw_read

    def connect_to_db(self):
        """
        Connect to postgres db
        """
        pghost, pgport, pgdatabase, pguser, pgpassword = self.parse_db_credentials()
        try:
            con = pg.connect(
                host=pghost,
                port=pgport,
                database=pgdatabase,
                user=pguser,
                password=pgpassword
            )
        except Exception as e:
            print('Unable to connect to database: '+pgdatabase)
            print(e)
        else:
            return con

    def pg_sql_to_df(self, sql_input):
        """
        Desc: load database query to pandas dataframe
        param con_db: database connection
        param sql: the sql query as one string [str]
        """
        con_pg = self.connect_to_db()
        try:
            df = pd.read_sql_query(sql=sql_input, con=con_pg)
        except Exception as e:
            print('SQL could not be executed')
            print(e)
        else:
            return df


def fetch_tablename_sqls():
    sql_tables_original = r'''
        WITH tmp AS 
            (
                SELECT table_name, regexp_matches(table_name, '(\d+)_(\d+)_(\d+)') AS regex
                FROM information_schema.TABLES
                WHERE table_schema = 'fremdrift'
                    AND table_name ILIKE 'fremdrift%'
            )
        SELECT   
            table_name 
        FROM tmp
        WHERE (regex)[1]::int = EXTRACT(YEAR FROM current_date)
        ORDER BY (regex)[2], (regex)[3]
        LIMIT 1
        ;
    '''

    sql_tables_recent = r'''
        WITH tmp AS 
            (
                SELECT table_name, regexp_matches(table_name, '(\d+)_(\d+)_(\d+)') AS regex
                FROM information_schema.TABLES
                WHERE table_schema = 'fremdrift'
                    AND table_name ILIKE 'fremdrift%'
            )
        SELECT   
            table_name
        FROM tmp
        WHERE (regex)[1]::int = EXTRACT(YEAR FROM current_date)
        ORDER BY (regex)[2] DESC, (regex)[3] DESC
        LIMIT 1
        ;
    '''

    return sql_tables_original, sql_tables_recent


def fetch_data_sql(tablename_original, tablename_recent):
    sql_original = f'''
        SELECT *
        FROM fremdrift.{tablename_original}
    '''

    sql_recent = f'''
        SELECT *
        FROM fremdrift.{tablename_recent}
    '''

    return sql_original, sql_recent


# set vars
year_current = datetime.now().year  # year e.g. 2022
month_current = datetime.now().strftime("%b")  # months in letter e.g. 'Jan'
day_current = "{:02}".format(datetime.now().day)  # day in 2 digits e.g. 01

# import database class and fetch sql as dataframe
d = Database()
sql_tablename_original, sql_tablename_recent = fetch_tablename_sqls()
df_tablename_original = d.pg_sql_to_df(sql_tablename_original)
tablename_original = str(df_tablename_original['table_name'][0])
df_tablename_recent = d.pg_sql_to_df(sql_tablename_recent)
tablename_recent = str(df_tablename_recent['table_name'][0])

sql_data_original, sql_data_recent = fetch_data_sql(tablename_original, tablename_recent)
df_data_original = d.pg_sql_to_df(sql_data_original)
df_data_recent = d.pg_sql_to_df(sql_data_recent)

prog_t = df_data_recent['months']
prog_iol = df_data_recent['cum_sum_prog']
mix_iol = df_data_recent['cum_sum_mix']
done_iol = df_data_recent['cum_sum_done']

static_prog_t = df_data_original['months']
static_prog_iol = df_data_original['cum_sum_prog']

# Plot results
fig, ax = plt.subplots()
fig.set_figheight(3)
fig.set_figwidth(6)
plt.plot_date(
    static_prog_t,
    static_prog_iol,
    ':', 
    color='red', 
    lw=5,
    label='Indvindingsoplande prognose (oprindelig)'
)

plt.plot_date(prog_t,
              mix_iol,
              '-', color='gray', lw=3,
              label='Indvindingsoplande prognose')

plt.plot_date(prog_t,
              done_iol,
              '-', color='black', lw=3,
              label='Indvindingsoplande')

plt.title(f'Fremdrift {year_current}')
plt.legend(loc='lower right', fontsize='xx-small')
# plt.show()

# Save plot to output file
root_path = 'F:/GKO/admin/ressourcer/fremdrift & enhedskontrakt/plots'
total_path = f'{root_path}/{year_current}/{month_current}'
if not os.path.exists(total_path):
    os.makedirs(total_path)

figur_path = os.path.join(f'{total_path}/fremdrift_{str(day_current)}.png')
plt.savefig(figur_path, dpi=900, transparent=True)
