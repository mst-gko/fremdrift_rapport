import os

import pandas as pd
import configparser
import psycopg2 as pg
from matplotlib import pyplot as plt
import numpy as np
from datetime import datetime


class Database:
    def __init__(self, database_name='GRUKOS', usr='writer'):
        self.ini_file = f'F:/GKO/data/grukos/db_credentials/{usr.lower()}/{usr.lower()}.ini'
        self.ini_database_name = database_name.upper()

    def parse_db_credentials(self):
        """
        fetches db credentials from ini file
        :return: dict containing db credentials
        """
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config[f'{self.ini_database_name}']['userid']
        pw_read = config[f'{self.ini_database_name}']['password']
        host = config[f'{self.ini_database_name}']['host']
        port = config[f'{self.ini_database_name}']['port']
        dbname = config[f'{self.ini_database_name}']['databasename']

        credentials = dict()
        credentials['usr'] = usr_read
        credentials['pw'] = pw_read
        credentials['host'] = host
        credentials['port'] = port
        credentials['dbname'] = dbname

        return credentials

    def connect_to_pgsql_db(self):
        """
        Connects to postgresql server database
        :return: postgresql psycopg2 connection and string database name
        """
        cred = self.parse_db_credentials()

        pg_database_name = cred['dbname']
        pg_con = None
        try:
            pg_con = pg.connect(
                host=cred['host'],
                port=cred['port'],
                database=cred['dbname'],
                user=cred['usr'],
                password=cred['pw']
            )
        except Exception as e:
            print(e)
        else:
            del cred
            # print(f'Connected to database: {pg_database_name}')

        return pg_con, pg_database_name

    def import_pgsql_to_df(self, sql):
        """
        Runs a query a mssql server database and store the output in a pandas dataframe
        :param sql: sql query string for the import
        :return: pandas dataframe containing output from sql query
        """
        conn, database = self.connect_to_pgsql_db()
        try:
            # print(f'Fetching table from {database} to dataframe...')
            df_pgsql = pd.read_sql(sql, conn)
            conn.close()
        except Exception as e:
            print(f'\n{e}')
            raise ValueError(f'Unable to fetch dataframe from database: {database}')
        else:
            # print(f'Table loaded into dataframe from database: {database}')
            pass

        return df_pgsql


def fetch_tablename_sql():
    sql_tables_recent = r'''
        WITH tmp AS 
            (
                SELECT table_name, regexp_matches(table_name, '(\d+)_(\d+)_(\d+)') AS regex
                FROM information_schema.TABLES
                WHERE table_schema = 'fremdrift'
                    AND table_name ILIKE 'indmeldinger%'
            )
        SELECT   
            table_name
        FROM tmp
        WHERE (regex)[1]::int = EXTRACT(YEAR FROM current_date)
        ORDER BY (regex)[2] DESC, (regex)[3] DESC
        LIMIT 1
        ;
    '''

    return sql_tables_recent


def fetch_data_sql(tablename_recent):
    sql_recent = f'''
        SELECT *
        FROM fremdrift.{tablename_recent}
        WHERE aar ~ '^[0-9\.]+$'
    '''

    return sql_recent


# set vars

year_current = datetime.now().year  # year e.g. 2022
month_current = datetime.now().strftime("%b")  # months in letter e.g. 'Jan'
day_current = "{:02}".format(datetime.now().day)  # day in 2 digits e.g. 01

# import the most recent data from the sql above and divide the data into variables
db = Database()
sql_tablename_recent = fetch_tablename_sql()
df_tablename_recent = db.import_pgsql_to_df(sql_tablename_recent)
tablename_recent = str(df_tablename_recent['table_name'][0])

sql_data_recent = fetch_data_sql(tablename_recent)
df_data_recent = db.import_pgsql_to_df(sql_data_recent)
df_data_recent = df_data_recent.astype({'aar': 'int'})  # specify integer of the years
years = df_data_recent['aar']
bar_data1 = df_data_recent['skalopgaver_aarligt']
bar_data2 = df_data_recent['afsluttede_skalopgaver_aarligt']
curve_data = df_data_recent['ikke_afsluttede_kumuleret']

# set up the figure
fig, ax = plt.subplots()

# to plot two bar plot next to each other on the same figure,
# the tick of the x-axis is changed from years to the quantity of years
# the position of first and second bar chart is shifted to the left and right respectively of the original tick
bar_width = 0.2  # set bar width
curve_positions = np.arange(len(years))  # change the year to the quantity of years
bar_positions1 = curve_positions - (0.5 * bar_width)  # shift the placement of the bar to the left of the x-axis tick
bar_positions2 = curve_positions + (0.5 * bar_width)  # shift the placement of the bar to the right of the x-axis tick

# Plot the two bar chart and the curve plot
ax.bar(bar_positions1, bar_data1, width=bar_width, align='center', label='Indmeldte', color='grey')
ax.bar(bar_positions2, bar_data2, width=bar_width, align='center', label='Afsluttede', color='k')
ax.plot(curve_positions, curve_data, label='Ikke-afsluttede, kumuleret', color='darkturquoise')

# set the x-axis ticks
ax.set_xticks(curve_positions)  # make the x-label tick every position of the curve, i.e. the non-shifted ticks
ax.set_xticklabels(years)  # insert the years as labels for the x-label ticks

# set labels and title
ax.set_xlabel('År')
ax.set_ylabel('Antal skal-opgaver')
ax.set_title('Status på indmeldte opgaver')

# generate legend, place legend below and make the curve the last of the legend items
handles, labels = plt.gca().get_legend_handles_labels()  # fetch the handles and labels of the plots
order = [1, 2, 0]  # set a new order of the legend items making the curve plot last on the legend
plt.legend(
    [handles[idx] for idx in order],
    [labels[idx] for idx in order],
    loc='upper center',
    bbox_to_anchor=(0.5, -0.15),
    ncol=3
)  # plot the legend below the plot
plt.subplots_adjust(bottom=0.2)  # adjust the plot such that the legend is displayed

# Save plot to output file
root_path = 'F:/GKO/admin/ressourcer/fremdrift & enhedskontrakt/plots'
total_path = f'{root_path}/{year_current}/{month_current}'
if not os.path.exists(total_path):
    os.makedirs(total_path)

figur_path = os.path.join(f'{total_path}/status_paa_indmeldte_opgaver_{str(day_current)}.png')
plt.savefig(figur_path, dpi=900)
